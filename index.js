/* 
    На мою думку, ми можемо використовувати блок try...catch у будь-яких випадках, коли нам необхідно, щоб код "не зламався", але при цьому зловити помилку. 
*/

const books = [
    {
        author: 'Люсі Фолі',
        name: 'Список запрошених',
        price: 70,
    },
    {
        author: 'Сюзанна Кларк',
        name: 'Джонатан Стрейндж і м-р Норрелл',
    },
    {
        name: 'Дизайн. Книга для недизайнерів.',
        price: 70,
    },
    {
        author: 'Алан Мур',
        name: 'Неономікон',
        price: 70,
    },
    {
        author: 'Террі Пратчетт',
        name: 'Рухомі картинки',
        price: 40,
    },
    {
        author: 'Анґус Гайленд',
        name: 'Коти в мистецтві',
    },
]

const root = document.querySelector('#root')
const list = document.createElement('ul')
root.append(list)

books.forEach((book) => {
    try {
        if (!book.author) {
            throw new Error(`В об'єкті немає властивісті 'author'`)
        }
        if (!book.name) {
            throw new Error(`В об'єкті немає властивісті 'name'`)
        }
        if (!book.price) {
            throw new Error(`В об'єкті немає властивісті 'price'`)
        }

        const listItem = document.createElement('li')
        listItem.innerHTML = `${book.author} - "${book.name}" - ${book.price} грн.`
        list.append(listItem)
    } catch (error) {
        console.error(error)
    }
})
